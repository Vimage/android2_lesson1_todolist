package com.vimage.mytodolist;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.vimage.mytodolist.data.TaskContract;
import com.vimage.mytodolist.data.TaskDBHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    ArrayList<TaskModel> tasks = new ArrayList<>();
    AdapterTasks tasksAdapter;
    CharSequence[] nChooseList = {"Perform task", "Edit task", "Delete task", "View performer tasks", "Delete all performer tasks"};
    int selectedTaskPosition;
    TaskModel currentTask;
    private TaskDBHelper taskDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.rvTasks);
        //fillArrayList();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        tasksAdapter = new AdapterTasks(tasks, MainActivity.this);
        recyclerView.setAdapter(tasksAdapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItemToTaskList();
//                Snackbar.make(view, "Task has been added to list", Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
            }
        });

        taskDbHelper = new TaskDBHelper(this);
        readTasks();

    }

    private void readTasks() {
        //чтение из БД сырым методом
        String query = "SELECT " + TaskContract.TaskEntry._ID + ", "
                + TaskContract.TaskEntry.COLUMN_DESCRIPTION + ", "
                + TaskContract.TaskEntry.COLUMN_DUE_TO_DATE + ", "
                + TaskContract.TaskEntry.COLUMN_PERFORM_DATE + ", "
                + TaskContract.TaskEntry.COLUMN_PERFORMER + " FROM " + TaskContract.TaskEntry.TABLE_NAME;
        SQLiteDatabase db = taskDbHelper.getReadableDatabase();
        Cursor cursor2 = db.rawQuery(query, null);
        while (cursor2.moveToNext()) {
            String id = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry._ID));
            String description = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_DESCRIPTION));

            long dueToDate = cursor2.getLong(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_DUE_TO_DATE));

            long performDate = cursor2.getLong(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_PERFORM_DATE));

            String performer = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_PERFORMER));

            fillTask(description, performer, dueToDate, performDate, id);

        }
        cursor2.close();
    }


    public void onTaskListItemClicked(View view) {
        int itemAdapterPosition = recyclerView.getChildAdapterPosition(view);
        if (itemAdapterPosition == RecyclerView.NO_POSITION) {
            return;
        }
        selectedTaskPosition = itemAdapterPosition;
        currentTask = tasks.get(selectedTaskPosition);
        tasksAdapter.setFocused(itemAdapterPosition);

    }

    public void onTaskListItemLongClicked(View view) {
        int itemAdapterPosition = recyclerView.getChildAdapterPosition(view);
        if (itemAdapterPosition == RecyclerView.NO_POSITION) {
            return;
        }
        selectedTaskPosition = itemAdapterPosition;
        currentTask = tasks.get(selectedTaskPosition);
        createListDialog(view);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_all) {

            clearView();

            readTasks();
            Toast.makeText(this, "All tasks view", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void clearView() {
        tasks.clear(); //clear list
        tasksAdapter.notifyDataSetChanged();
    }

    public void addItemToTaskList() {
        createCustomCodeDialog(null);
    }

    public void createTask(String description, String performer, long dueToDate) {
        currentTask = new TaskModel();
        currentTask.description = description;
        currentTask.performer = performer;
        ////currentTask.description = "Новая задача";
        currentTask.dueToDate = dueToDate;
        currentTask.performDate = System.currentTimeMillis();
        tasksAdapter.addItemAtLastPosition(currentTask);
        //createCustomCodeDialog(null);
        insertTaskToDB(currentTask);

    }

    public void fillTask(String description, String performer, long dueToDate, long performDate, String id) {
        currentTask = new TaskModel();
        currentTask.description = description;
        currentTask.performer = performer;
        ////currentTask.description = "Новая задача";
        currentTask.dueToDate = dueToDate;
        currentTask.performDate = performDate;
        currentTask.id = id;

        tasksAdapter.addItemAtLastPosition(currentTask);
    }

    private void insertTaskToDB(TaskModel currentTask) {
        //вставка записи не через чистый запрос
        SQLiteDatabase db = taskDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TaskContract.TaskEntry.COLUMN_DESCRIPTION, currentTask.description);
        values.put(TaskContract.TaskEntry.COLUMN_DUE_TO_DATE, currentTask.dueToDate);
        values.put(TaskContract.TaskEntry.COLUMN_PERFORM_DATE, currentTask.performDate);
        values.put(TaskContract.TaskEntry.COLUMN_PERFORMER, currentTask.performer);

        long newRowId = db.insert(TaskContract.TaskEntry.TABLE_NAME, null, values);

        if (newRowId == -1) {
            // произошла ошибка
            Toast.makeText(this, "Ошибка при заведении", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Задача вставлена в таблицу c # " + newRowId, Toast.LENGTH_SHORT).show();
        }
    }


    private void updateTaskInDB(TaskModel currentTask) {
        //редактирование строки в БД записи не через чистый запрос
        SQLiteDatabase db = taskDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TaskContract.TaskEntry.COLUMN_DESCRIPTION, currentTask.description);
        values.put(TaskContract.TaskEntry.COLUMN_DUE_TO_DATE, currentTask.dueToDate);
        values.put(TaskContract.TaskEntry.COLUMN_PERFORM_DATE, currentTask.performDate);
        values.put(TaskContract.TaskEntry.COLUMN_PERFORMER, currentTask.performer);

        db.update(TaskContract.TaskEntry.TABLE_NAME, values, "_id = ?", new String[]{currentTask.id});

    }

    private void deleteTaskInDB(TaskModel currentTask) {
        //редактирование строки в БД записи не через чистый запрос
        SQLiteDatabase db = taskDbHelper.getWritableDatabase();
        db.delete(TaskContract.TaskEntry.TABLE_NAME, "_id = ?", new String[]{currentTask.id});

    }

    public void createCustomCodeDialog(View view) {

        //final View viewForDialog = createViewForDialog(currButton.getText().toString());
        final View viewForDialog = createViewForDialog("", "");
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Enter task properties")
                .setView(viewForDialog)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Create task", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) ((LinearLayout) viewForDialog).getChildAt(0);//(EditText) viewForDialog.findViewById(123);
                        EditText performerText = (EditText) ((LinearLayout) viewForDialog).getChildAt(1);
                        EditText dateText = (EditText) ((LinearLayout) viewForDialog).getChildAt(2);
                        //dateText.


                        Date date = null;
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            date = format.parse(String.valueOf(dateText.getText()));

                            //System.out.println(date);

                        } catch (ParseException e) {

                            e.printStackTrace();
                        }
                        //Toast.makeText(MainActivity.this, date.toString(), Toast.LENGTH_SHORT).show();
                        createTask(editText.getText().toString(), performerText.getText().toString(), date.getTime());

                    }
                })
                .setCancelable(false)
                .show();
    }


    public void createCustomCodeDialogForEdit(View view) {

        //final View viewForDialog = createViewForDialog(currButton.getText().toString());
        final View viewForDialog = createViewForDialog(currentTask.description, currentTask.performer);
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Enter task properties")
                .setView(viewForDialog)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Apply changes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText editText = (EditText) ((LinearLayout) viewForDialog).getChildAt(0);//(EditText) viewForDialog.findViewById(123);
                        EditText performerText = (EditText) ((LinearLayout) viewForDialog).getChildAt(1);
                        EditText dateText = (EditText) ((LinearLayout) viewForDialog).getChildAt(2);
                        //dateText.


                        Date date = null;
                        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            date = format.parse(String.valueOf(dateText.getText()));

                            //System.out.println(date);

                        } catch (ParseException e) {

                            e.printStackTrace();
                        }
                        //Toast.makeText(MainActivity.this, date.toString(), Toast.LENGTH_SHORT).show();
                        editTask(editText.getText().toString(), performerText.getText().toString(), date.getTime());

                    }
                })
                .setCancelable(false)
                .show();
    }

    public void editTask(String description, String performer, long dueToDate) {
        //currentTask = new TaskModel();
        currentTask.description = description;
        currentTask.performer = performer;
        currentTask.dueToDate = dueToDate;
        currentTask.performDate = System.currentTimeMillis();
        tasksAdapter.changeItemAtPosition(selectedTaskPosition);
        updateTaskInDB(currentTask);
        //createCustomCodeDialog(null);


    }


    private View createViewForDialog(String editingText, String performer) {
        LinearLayout linearLayout = new LinearLayout(MainActivity.this);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams layoutParamsForView = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParamsForView.gravity = Gravity.CENTER_HORIZONTAL;

        EditText editText = new EditText(MainActivity.this);
        if (editingText.equals("")) {
            editText.setHint("Enter task description");
        } else {
            editText.setText(editingText);
        }
        editText.setTextSize(20);
        linearLayout.addView(editText, layoutParamsForView);

        EditText editTextPerformer = new EditText(MainActivity.this);
        if (performer.equals("")) {
            editTextPerformer.setHint("Enter performer name");
        } else {
            editTextPerformer.setText(performer);
        }
        editTextPerformer.setTextSize(20);
        linearLayout.addView(editTextPerformer, layoutParamsForView);


// получим текущую дату
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        final EditText editDate = new EditText(MainActivity.this);
        editDate.setText(day + "-" + (month) + "-" + year);
        editDate.setInputType(InputType.TYPE_NULL);
        //editDate.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
        editDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createDatePickerDialog(null, editDate);


            }
        });

        linearLayout.addView(editDate, layoutParamsForView);


        return linearLayout;
    }

    DialogInterface.OnClickListener dialogIntfListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            switch (i) {
                case 0:
                    tasksAdapter.setPerormed(selectedTaskPosition);
                    //updateTaskInDB(currentTask);
                    //createCustomCodeDialog("fggggggggggggg");
                    //Toast.makeText(MainActivity.this, "0", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    createCustomCodeDialogForEdit(null);
                    //createCustomCodeDialog(null);
                    //deleteButton(currButton);
                    break;

                case 2:
                    deleteTaskInDB(currentTask); // удаляем выбранную задачу
                    tasksAdapter.removeItemAtPosition(selectedTaskPosition);
                    ////deleteButton(currButton);
                    break;
                case 3: // выборка всех задач по исполнителю
                    clearView();
                    selectAllPerformerTasksInDB(currentTask);
                    break;
                case 4: //удаление всех задач по исполнителю

                    deleteAllPerformerTasks(currentTask);
                    break;
                default:


                    //Toast.makeText(MainActivity.this, nChooseList[i], Toast.LENGTH_SHORT).show();
            }
        }
    };

    private boolean deleteAllPerformerTasks(final TaskModel task) {

        AlertDialog.Builder ad;
        ad = new AlertDialog.Builder(MainActivity.this);
        ad.setTitle("All performer tasks delete");  // заголовок
        ad.setMessage("All " + task.performer + "'s tasks will be deleted.\nPlease, confirm."); // сообщение
        ad.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        try {
                            deleteAllPerformerTasksInDB(task);
                        } finally {
                            clearView();
                            readTasks();
                            Toast.makeText(MainActivity.this, "All " + task.performer + "'s tasks deleted.",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );

        ad.setNegativeButton("NO", new DialogInterface.OnClickListener()

                {
                    public void onClick(DialogInterface dialog, int arg1) {

                    }
                }

        );
        ad.setCancelable(true);
        ad.show();
        return false;
    }

    private void deleteAllPerformerTasksInDB(TaskModel task) {
        //удаление из БД сырым методом
        String query = "DELETE FROM " + TaskContract.TaskEntry.TABLE_NAME +
                " WHERE performer = '" + task.performer + "'";
        SQLiteDatabase db = taskDbHelper.getWritableDatabase();
        db.execSQL(query);
    }

    private void selectAllPerformerTasksInDB(TaskModel task) {
        //чтение из БД сырым методом
        String query = "SELECT " + TaskContract.TaskEntry._ID + ", "
                + TaskContract.TaskEntry.COLUMN_DESCRIPTION + ", "
                + TaskContract.TaskEntry.COLUMN_DUE_TO_DATE + ", "
                + TaskContract.TaskEntry.COLUMN_PERFORM_DATE + ", "
                + TaskContract.TaskEntry.COLUMN_PERFORMER + " FROM " + TaskContract.TaskEntry.TABLE_NAME +
                " WHERE performer = '" + task.performer + "'";
        SQLiteDatabase db = taskDbHelper.getReadableDatabase();
        Cursor cursor2 = db.rawQuery(query, null);
        while (cursor2.moveToNext()) {
            String id = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry._ID));
            String description = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_DESCRIPTION));

            long dueToDate = cursor2.getLong(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_DUE_TO_DATE));

            long performDate = cursor2.getLong(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_PERFORM_DATE));

            String performer = cursor2.getString(cursor2
                    .getColumnIndex(TaskContract.TaskEntry.COLUMN_PERFORMER));

            fillTask(description, performer, dueToDate, performDate, id);

        }
        cursor2.close();

    }

    public void createListDialog(View view) {


        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Choose action")
                .setItems(nChooseList, dialogIntfListener)
                .setNeutralButton("Cancel", null)
                .show();

    }

    public void createDatePickerDialog(View view, final EditText editDate) {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        //String selDate = "";

        new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                //Toast.makeText(MainActivity.this, dayOfMonth + "-" +monthOfYear + "-" + year, Toast.LENGTH_SHORT).show();
                editDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
            }
        }, year, month, day).
                show();


    }


}
