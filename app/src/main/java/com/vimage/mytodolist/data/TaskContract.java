package com.vimage.mytodolist.data;

/**
 * Created by Dimon on 18.12.2016.
 */
import android.provider.BaseColumns;
public class TaskContract {

    private TaskContract() {
    }

    public static final class TaskEntry implements BaseColumns {
        public final static String TABLE_NAME = "tasks";

        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_DESCRIPTION = "description";
        public final static String COLUMN_PERFORMER = "performer";
        public final static String COLUMN_DUE_TO_DATE = "dueToDate";
        public final static String COLUMN_PERFORM_DATE = "performDate";

    }

}
