package com.vimage.mytodolist;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;


/**
 * Created by Dimon on 17.12.2016.
 */
public class AdapterTasks extends RecyclerView.Adapter<TaskHolder> {

    ArrayList<TaskModel> arrayList;
    private MainActivity mainActivity;

    public AdapterTasks(ArrayList<TaskModel> arrayList, MainActivity mainActivity) {
        this.arrayList = arrayList;
        this.mainActivity = mainActivity;
    }


    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_list_item, parent, false);
        TaskHolder taskHolder = new TaskHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.onTaskListItemClicked(view);
            }
        });

        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mainActivity.onTaskListItemLongClicked(view);
                return true;
            }
        });


        return taskHolder;
    }

    @Override
    public void onBindViewHolder(TaskHolder holder, int position) {
        TaskModel taskModel = arrayList.get(position);
        holder.tvTaskDescription.setText(taskModel.description);
        holder.tvPerformer.setText(taskModel.performer);

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        formatter.setLenient(false);
        String curTime = formatter.format(taskModel.dueToDate);
        holder.tvDueToDate.setText(curTime);

        //SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        formatter.setLenient(false);
        String perfTime = formatter.format(taskModel.performDate);
        holder.tvPerformDate.setText(perfTime);

        holder.rlBg.setBackgroundColor(taskModel.bgColor);
        if (!taskModel.isPerformed) {
            holder.tvPerformDate.setVisibility(View.INVISIBLE);
            holder.tvPerformDateLabel.setVisibility(View.INVISIBLE);
        } else {
            holder.tvPerformDate.setVisibility(View.VISIBLE);
            holder.tvPerformDateLabel.setVisibility(View.VISIBLE);
        }


        //holder.tvDueToDate.setText(taskModel.dueToDate);
        Context context = holder.ivIcon.getContext();
        //if(taskModel.avatar)
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//                holder.ivIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.man_100_100, context.getTheme()));
//            else
//                holder.ivAvatar.setImageDrawable(context.getResources().getDrawable(R.drawable.man_100_100));


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void removeItemAtPosition(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
    }


    public void addItemAtLastPosition(TaskModel taskModel) {

        arrayList.add(taskModel);
        mainActivity.recyclerView.scrollToPosition(arrayList.size() - 1);
        notifyItemInserted(arrayList.size() - 1);
    }

    public void changeItemAtPosition(int position) {
        //arrayList.set(position, ColorsHelper.getRandomColor());
        notifyItemChanged(position);
    }


    public void setFocused(int position) {
        //arrayList.get(position).bgColor = Color.CYAN;
        notifyItemChanged(position);


    }

    public void setPerormed(int position) {

        arrayList.get(position).bgColor = mainActivity.getResources().getColor(R.color.colorGreen);
        arrayList.get(position).isPerformed = true;
        arrayList.get(position).performDate = System.currentTimeMillis();
        notifyItemChanged(position);


    }


}
